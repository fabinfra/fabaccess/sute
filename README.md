This project was archived at 28.02.2025 as it is deprecated for the following reasons:
- wrong .gitmodules config - belongs to a really old status of bffh project
- no readme
- no usage examples
- does not compile anymore
- no commits since 2021