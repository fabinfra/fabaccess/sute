use clap::{
    App,
    AppSettings,
    Arg,
    ArgMatches,
    Error,
};

pub enum Commands {

}

pub struct CommandParser<'help> {
    app: App<'help>,
}

impl<'help> CommandParser<'help> {
    pub fn new() -> Self {
        Self { 
            app: App::new("Commands")
                .setting(AppSettings::DisableVersion)
                .setting(AppSettings::StrictUtf8)
                .setting(AppSettings::ColorAlways)
                .setting(AppSettings::NoBinaryName)
                .subcommand(App::new("quit"))
                .subcommand(App::new("connect"))
                .subcommand(App::new("authenticate")
                    .arg(Arg::new("user")
                        .takes_value(true))
                    .arg(Arg::new("pass")
                        .takes_value(true)))
                .subcommand(App::new("minfo")
                    .arg(Arg::new("uid")
                        .takes_value(true)))
                .subcommand(App::new("list"))
                .subcommand(App::new("use")
                    .arg(Arg::new("uid")
                        .takes_value(true)))
                ,
        }
    }

    pub fn parse<I: IntoIterator<Item = String>>(&mut self, iter: I) -> Result<ArgMatches, Error> {
        self.app.clone().try_get_matches_from(iter)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_commands_test() {
        let mut cmds = CommandParser::new();
        let p = vec!["connect".to_string()];
        let matches = cmds.parse(p).unwrap();

        assert_eq!(matches.subcommand_name(), Some("connect"));
    }

    #[test]
    fn fail_parsing_test() {
        let mut cmds = CommandParser::new();
        let p = vec!["invalid".to_string()];
        let matches = cmds.parse(p);

        assert!(matches.is_err())
    }
}
