use std::sync::Arc;

use tui::{
    backend::Backend,
    layout::{Layout, Direction, Constraint, Rect},
    widgets::{Paragraph, Block, Borders, Clear},
    Frame,
};

use crate::app::{SuteState, LogDrain};

pub struct UIState<'a> {
    pub app_state: SuteState,
    pub loglines: Arc<LogDrain<'a>>,
}
impl<'a> UIState<'a> {
    pub fn new(app_state: SuteState, loglines: Arc<LogDrain<'a>>) -> Self {
        Self { app_state, loglines }
    }
}

pub fn draw_ui<B: Backend>(f: &mut Frame<B>, state: &mut UIState) {
    let outer_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
                Constraint::Length(6),
                Constraint::Min(1),
                Constraint::Length(3),
            ].as_ref(),
        )
        .split(f.size());


    draw_header(f, state, outer_layout[0]);
    draw_main(f, state, outer_layout[1]);
    draw_command_line(f, state, outer_layout[2]);
}

fn draw_header<B: Backend>(f: &mut Frame<B>, state: &mut UIState, layout_chunk: Rect) {
    f.render_widget(Block::default()
        .title("Status")
        .borders(Borders::ALL), layout_chunk);
}

fn draw_main<B: Backend>(f: &mut Frame<B>, state: &mut UIState, layout_chunk: Rect) {
   // let chunk = Layout::default()
   //     .direction(Direction::Horizontal)
   //     .constraints([Constraint::Percentage(20), Constraint::Percentage(80)].as_ref())
   //     .split(layout_chunk);

    draw_logs(f, state, layout_chunk)
}
fn draw_logs<B: Backend>(f: &mut Frame<B>, state: &mut UIState, layout_chunk: Rect) {
    // TODO: Just use a signal.
    let v = state.loglines.get_inner().clone();
    f.render_widget(Paragraph::new(v), layout_chunk);
}

fn draw_command_line<B: Backend>(f: &mut Frame<B>, state: &mut UIState, layout_chunk: Rect) {
    let block = Block::default()
        .title("Command line")
        .borders(Borders::ALL);
    let inner_rect = block.inner(layout_chunk);

    f.render_widget(block, layout_chunk);

    let cmdline = format!("{} > {}", state.app_state.tick_c, state.app_state.cmd_line);
    f.render_widget(Paragraph::new(&cmdline[..]), inner_rect);
}
