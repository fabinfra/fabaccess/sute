use std::fmt;
use std::any::Any;
use std::future::Future;

use futures::FutureExt;

use super::auth_capnp::authentication::Client;

pub struct Authentication {
    inner: Client,
}
impl fmt::Debug for Authentication {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Authentication")
            .field("inner", &self.inner.type_id())
            .finish()
    }
}

impl Authentication {
    pub fn new(inner: Client) -> Self {
        Self { inner }
    }

    pub fn mechanisms(&mut self) -> impl Future<Output=Vec<String>> {
        let req = self.inner.mechanisms_request().send().promise;
        req.map(|res| {
            let res = res.unwrap();
            let tmp = res.get().unwrap();
            tmp.get_mechs().unwrap().iter().map(|x| x.unwrap().to_string()).collect()
        })
    }

    pub fn authenticate(&mut self, username: String, password: String) -> impl Future<Output=bool> {
        let mut req = self.inner.start_request();
        let mut builder = req.get().init_request();
        builder.set_mechanism("PLAIN");
        let mut init_data = builder.init_initial_response();
        init_data.set_initial(format!("\0{}\0{}", username, password).as_ref());
        let response = req.send().promise;
        response.map(|res| {
            let res = res.unwrap();
            let tmp = res.get().unwrap().get_response().unwrap();
            match tmp.which().unwrap() {
                super::auth_capnp::response::Which::Challence(_) => false,
                super::auth_capnp::response::Which::Outcome(outcome) => {
                    if outcome.get_result().unwrap() == super::auth_capnp::response::Result::Successful {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        })
    }
}
