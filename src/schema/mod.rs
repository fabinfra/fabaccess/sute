use std::future::Future;
use std::io;

use slog::Logger;

use smol::net::TcpStream;

use capnp_rpc::twoparty;
use capnp_rpc::RpcSystem;
use capnp_rpc::rpc_twoparty_capnp::Side;

#[allow(dead_code)]
mod auth_capnp {
    include!(concat!(env!("OUT_DIR"), "/schema/auth_capnp.rs"));
}
#[allow(dead_code)]
mod connection_capnp {
    include!(concat!(env!("OUT_DIR"), "/schema/connection_capnp.rs"));
}
#[allow(dead_code)]
mod api_capnp {
    include!(concat!(env!("OUT_DIR"), "/schema/api_capnp.rs"));
}

mod api;
pub use api::API;

mod authentication;
pub use authentication::Authentication;

mod machines;
pub use machines::Machines;

mod machine;
pub use machine::Machine;

pub fn bootstrap(log: Logger, stream: TcpStream) -> (RpcSystem<Side>, API) {
    let network = Box::new(twoparty::VatNetwork::new(stream.clone(), stream,
        Side::Client, Default::default()));

    let mut rpc = RpcSystem::new(network, None);
    let client: connection_capnp::bootstrap::Client 
        = rpc.bootstrap(Side::Server);

    return (rpc, API::new(log, client));
}
