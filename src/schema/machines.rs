use std::str::FromStr;
use std::fmt;
use std::any::Any;
use std::future::Future;
use futures::FutureExt;

use super::api_capnp::machines::Client;

use super::machine::Machine;

use uuid::Uuid;

pub struct Machines {
    inner: Client,
}
impl fmt::Debug for Machines {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Machines")
            .field("inner", &self.inner.type_id())
            .finish()
    }
}

impl Machines {
    pub fn new(inner: Client) -> Self {
        Self { inner }
    }

    pub fn get_machine(&mut self, uid: String) -> impl Future<Output=Option<Machine>> {
        let mut req = self.inner.get_machine_request();
        let mut param_builder = req.get();
        let uid_builder = param_builder.set_uid(uid.as_ref());

        let prom = req.send().promise;

        // TODO: When's that an Err?
        prom.map(|res| {
            // TODO: When's that an Err?
            let tmp = res.unwrap();
            let moretmp = tmp.get().unwrap();
            if let Ok(m) = moretmp.get_machine() {
                let read = m.get_read().map_err(|e| {
                    println!("get_machine->get_read Err: {}", e);
                }).unwrap();

                let write = m.get_write().map_err(|e| {
                    println!("get_machine->get_write Err: {}", e);
                }).unwrap();

                return Some(Machine::new(read, write));
            }
            None
        })
    }

    pub fn list_machines(&mut self) -> impl Future<Output=Vec<Machine>> {
        let req = self.inner.list_machines_request();
        let promise = req.send().promise;

        promise.map(|res| {
            let tmp = res.unwrap();
            let moretmp = tmp.get().unwrap();
            let mut out = Vec::new();
            if let Ok(m) = moretmp.get_machines() {
                for machine in m.iter() {
                    if let Ok(read) = machine.get_read() {
                        if let Ok(write) = machine.get_write() {
                            out.push(Machine::new(read, write));
                        }
                    }
                }
            } else {
                println!("No get_machines? :(")
            }
            return out;
        })
    }
}

pub fn uuid_from_api(uuid: crate::schema::api_capnp::u_u_i_d::Reader) -> Uuid {
    let uuid0 = uuid.get_uuid0() as u128;
    let uuid1 = uuid.get_uuid1() as u128;
    let num: u128 = (uuid0 << 64) + uuid1;
    Uuid::from_u128(num)
}
pub fn api_from_uuid(uuid: Uuid, mut wr: crate::schema::api_capnp::u_u_i_d::Builder) {
    let num = uuid.as_u128();
    let uuid0 = num as u64;
    let uuid1 = (num >> 64) as u64;
    wr.set_uuid0(uuid0);
    wr.set_uuid1(uuid1);
}
