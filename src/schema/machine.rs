use std::fmt;
use std::any::Any;
use std::future::Future;
use futures::FutureExt;

use super::api_capnp::machine::{
    read::{Client as ReadClient},
    write::{Client as WriteClient},
    write::give_back::{Client as GiveBackClient},
};

pub struct Machine {
    read: ReadClient,
    write: WriteClient
}
impl fmt::Debug for Machine {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut b = f.debug_struct("Machine");
        b.field("read", &self.read.type_id());
        b.finish()
    }
}

impl Machine {
    pub fn new(read: ReadClient, write: WriteClient) -> Self {
        Machine {
            read,
            write,
        }
    }

    pub fn get_info(&self) -> impl Future<Output=Option<String>> {
        let req = self.read.info_request().send().promise;
        req.map(|res| {
            res.unwrap().get().unwrap().get_minfo().ok().map(|minfo| {
                // TODO state
                format!("Name: {:?}\n Description: {:?}\nState: ",
                    minfo.get_name(), minfo.get_description())
            })
        })
    }

    pub fn use_(&self) -> impl Future<Output=Option<GiveBack>> {
        let req = self.write.use_request().send().promise;

        req.map(|res| {
            res.unwrap().get().unwrap().get_ret().ok().map(|gb| {
                GiveBack::new(gb)
            })
        })
    }
}

pub struct GiveBack {
    client: GiveBackClient
}

impl GiveBack {
    pub fn new(client: GiveBackClient) -> Self {
        Self { client }
    }

    pub async fn give_back(self) {
        self.client.ret_request().send().promise.await;
    }
}
