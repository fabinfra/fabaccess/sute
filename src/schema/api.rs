use std::fmt;
use std::future::Future;
use futures::FutureExt;

use slog::Logger;

use super::connection_capnp::bootstrap::Client;
use super::Authentication;
use super::Machines;

#[derive(Clone)]
pub struct API {
    inner: Client,
    log: Logger,
}

impl API {
    pub fn new(log: Logger, inner: Client) -> Self {
        Self { log, inner}
    }

    pub fn authentication(&mut self) -> impl Future<Output=Authentication> {
        let req = self.inner.auth_request().send().promise;
        // TODO: When's that an Err?
        req.map(|res| {
            // TODO: When's that an Err?
            let tmp = res.unwrap();
            let moretmp = tmp.get().unwrap();
            Authentication::new(moretmp.get_auth().unwrap())
        })
    }

    pub fn machines(&mut self) -> impl Future<Output=Machines> {
        let req = self.inner.machines_request().send().promise;
        // TODO: When's that an Err?
        req.map(|res| {
            // TODO: When's that an Err?
            let tmp = res.unwrap();
            let moretmp = tmp.get().unwrap();
            Machines::new(moretmp.get_machines().unwrap())
        })
    }
}
