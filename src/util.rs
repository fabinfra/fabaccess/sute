use std::io;
use std::pin::Pin;

use futures::prelude::*;
use futures::ready;
use futures::task::{Context, Poll};
use futures_signals::signal::Signal;


pub struct Resize {
    inner: smol::net::unix::UnixStream,
    size: (u16, u16),
}

impl Resize {
    pub fn new() -> Result<Self, io::Error> {
        let size = termion::terminal_size()?;
        let (a, inner) = smol::net::unix::UnixStream::pair()?;
        signal_hook::pipe::register(signal_hook::SIGWINCH, a)?;
        Ok(Self { inner, size })
    }
}

impl Signal for Resize {
    type Item = (u16,u16);

    fn poll_change(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        if let Err(e) = ready!(AsyncRead::poll_read(Pin::new(&mut self.inner), cx, &mut [0])) {
            println!("Error in checking signals: {}", e);

            Poll::Ready(None)
        } else {
            match termion::terminal_size() {
                Ok(s) => {
                    self.size = s;
                    Poll::Ready(Some(s))
                }
                Err(e) => {
                    println!("Error in checking signals: {}", e);
                    Poll::Ready(None)
                }
            }
        }
    }
}
