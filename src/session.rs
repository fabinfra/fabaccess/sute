
use anyhow::Result;

use capnp_rpc::RpcSystem;
use capnp_rpc::rpc_twoparty_capnp::Side;

use smol::net::TcpStream;
use smol::net::AsyncToSocketAddrs;

use slog::Logger;

use crate::schema::{bootstrap, API, Authentication};

pub struct Session {
    pub bootstrap: API,
    authenticate: Option<Authentication>,
    pub vat: Option<RpcSystem<Side>>,
}

impl Session {
    pub async fn connect<A: AsyncToSocketAddrs>(log: Logger, addr: A) -> Result<Session> {
        let stream = TcpStream::connect(addr).await?;
        let (rpc_system, api) = bootstrap(log, stream);

        Ok(Session {
            bootstrap: api,
            authenticate: None,
            vat: Some(rpc_system),
        })
    }
}
