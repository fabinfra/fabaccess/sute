use std::{mem, io, thread};
use std::pin::Pin;
use std::task::{Context, Poll};

use termion::event::Key;
use termion::input::TermRead;

use futures::Stream;
use futures::ready;
use futures::channel::mpsc;
use futures::SinkExt;

pub struct Inputs {
    rx: mpsc::Receiver<Key>,
    hndl: thread::JoinHandle<()>,
}

impl Inputs {
    pub fn new() -> Self {
        let (mut tx, rx) = mpsc::channel(64);

        let hndl = thread::spawn(move || {
            let stdin = io::stdin();
            let mut keys = stdin.keys();
            for key in keys {
                let key = key.unwrap();
                smol::block_on(tx.send(key));
            }
        });

        Self {
            rx: rx,
            hndl: hndl,
        }
    }
}

impl Stream for Inputs {
    type Item = Key;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        Pin::new(&mut self.rx).poll_next(cx)
    }
}
