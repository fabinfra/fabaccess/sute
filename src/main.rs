#[macro_use]
extern crate slog;

#[macro_use]
extern crate futures;

use std::io;
use std::sync::{Arc, Mutex};
use std::thread;

use smol::Task;

use tui::backend::{Backend, TermionBackend};
use tui::Terminal;
use termion::raw::IntoRawMode;
use termion::input::TermRead;
use termion::event::Key;

use futures::StreamExt;
use futures_signals::signal::SignalExt;

use clap::{App, Arg};

mod banner;
mod config;
mod app;
mod input;
mod util;
mod ui;
mod schema;
mod session;
mod commands;

use banner::BANNER;

static DEFAULT_SERVER: &'static str = "localhost:59661";

fn main() -> Result<(), io::Error> {

    let matches = App::new("sute 🌸")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .before_help(BANNER)
        .arg(Arg::new("config")
            .short('c')
            .long("config")
            .about("Specify configuration file path")
            .takes_value(true))
        .arg(Arg::new("server")
            .short('s')
            .long("server")
            .about("Connect to the specified address[:port] as server")
            .takes_value(true))
        .get_matches();

    // 
    let server = matches.value_of("server").unwrap_or(DEFAULT_SERVER);

    // Set up logging
    let drain = Arc::new(app::LogDrain::new());
    let log = slog::Logger::root(slog::Fuse::new(drain.clone()), o!());

    let lex = smol::LocalExecutor::new();

    let resize = util::Resize::new()?;

    let session_f = session::Session::connect(log.clone(), server);

    let mut session = smol::block_on(session_f).unwrap();
    let rpc_future = session.vat.take().unwrap();

    let app = app::Sute::new(resize, log.clone(), drain.clone(), Some(session));

    let app_state = app.get_state();
    let mut ui_state = ui::UIState::new(app_state, drain);

    let signal = app.signal();

    let ui_future = async move {
        let stdout = io::stdout().into_raw_mode()?;
        let backend = TermionBackend::new(stdout);
        let mut terminal = Terminal::new(backend)?;
        terminal.hide_cursor()?;

        let mut tick_c = '|';

        // Refresh the screen once by resizing the terminal
        if let Ok((x,y)) = termion::terminal_size() {
            terminal.resize(tui::layout::Rect::new(0, 0, x,y)).unwrap();
        }

        terminal.draw(|f| ui::draw_ui(f, &mut ui_state))?;
        let mut stream = signal.to_stream();
        loop {
            if let Some(mut state) = stream.next().await {
                if !state.running {
                    break;
                }

                tick_c = match tick_c {
                    '\\' => '|',
                    '|'  => '/',
                    '/'  => '-',
                    '-'  => '\\',
                    _ => '|',
                };
                state.tick_c = tick_c;
                ui_state.app_state = state;
                terminal.draw(|f| ui::draw_ui(f, &mut ui_state))?; 
            } else {
                break;
            }
        }

        // TODO: Ensure that will always be run
        terminal.show_cursor()?;

        Ok(())
    };

    crit!(log, "Critical log line");
    error!(log, "Error log line");
    warn!(log, "Warning log line");
    info!(log, "Informational log line");
    debug!(log, "Debugging log line");
    trace!(log, "Tracing log line");

    lex.spawn(rpc_future).detach();
    let t: Task<Result<(), io::Error>> = lex.spawn(ui_future);
    t.detach();
    smol::block_on(lex.run(app));

    Ok(())
}
